<?php namespace juliorafaelr\DateHelper;

use DateInterval;
use DateTime;
use DateTimeZone;

class DateHelper {
	/**
	 *
	 *
	 * @param $startDate
	 * @param $endDate
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */

	public static function getDatesByRange( string $startDate, string $endDate ): array {
		if ( $startDate > $endDate ) {
			throw new \Exception ( 'start date cannot be bigger than end date' );
		}

		$dates = array( );

		$date = new DateTime( $startDate );

		$dateInterval = new DateInterval( 'P1D' );

		while( $startDate <= $endDate ) {
			$dates[ ] = $date->format( 'Y-m-d' );

			$date->add( $dateInterval );

			$startDate = $date->format( 'Y-m-d' );
		}

		asort( $dates );

		return $dates;
	}

	/**
	 *
	 *
	 * @param $startDate
	 * @param $endDate
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */

	public static function getDatesByRangeInverse( string $startDate, string $endDate ): array {
		if ( $startDate > $endDate ) {
			throw new \Exception ( 'start date cannot be bigger than end date' );
		}

		$dates = array( );

		$date = new DateTime( $endDate );

		$dateInterval = new DateInterval( 'P1D' );

		while( $endDate >= $startDate ) {
			$dates[ ] = $date->format( 'Y-m-d' );

			$date->sub( $dateInterval );

			$endDate = $date->format( 'Y-m-d' );
		}

		return $dates;
	}

	/**
	 *
	 *
	 * @param string $startDate
	 * @param string $endDate
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */

	public static function getMonthsByRange( string $startDate, string $endDate ): array {
		if ( $startDate > $endDate ) {
			throw new \Exception ( 'start date cannot be bigger than end date' );
		}

		$dates = array( );

		$date = new DateTime( $endDate );

		$dateInterval = new DateInterval( 'P1M' );

		while( $startDate <= $endDate ) {
			$dates[ ] = $date->format( 'm_Y' );

			$date->sub( $dateInterval );

			$endDate = $date->format( 'Y-m-d' );
		}

		return $dates;
	}

	/**
	 *
	 *
	 * @param int $ms
	 *
	 * @return array
	 *
	 * @throws \Exception
	 */

	public static function msToTime( int $ms ): array {
		$seconds = (int)( $ms / 1000 );

		$dtF = new DateTime( '@0' );

		$dtT = new DateTime("@$seconds");

		$temp = explode( "-", $dtF->diff($dtT)->format( '%a-%h-%i-%s' ) );

		$time = [
			'days'    => $temp[ 0 ],
			'hours'   => $temp[ 1 ],
			'minutes' => $temp[ 2 ],
			'seconds' => $temp[ 3 ],
		];

		return $time;
	}

	/**
	 *
	 *
	 * @param string $date
	 *
	 * @return string|null
	 */

	public static function parseDateToTimeStamp( string $date ): ?string {
		if( empty( $date ) === false ) {
			$string = date( 'Y-m-d H:i:s', strtotime( $date ) );

			if( $string === false ) {
				return null;
			} else {
				return $string;
			}

		}
		else {
			return null;
		}
	}

	/**
	 *
	 *
	 * @param string $date
	 * @param string|null $timeZone
	 *
	 * @return false|string
	 *
	 * @throws \Exception
	 */

	public static function parseDateToZulu( string $date, ?string $timeZone = null ) {
		if( empty( $timeZone ) === false ) {
			$time_zone = new DateTimeZone( $timeZone );

			$dt = new DateTime( $date ,$time_zone );

			return $dt->format( 'Y-m-d\TH:i:sO' );
		}
		else {
			return date( 'Y-m-d\TH:i:s\Z', strtotime( $date ) );
		}
	}

	/**
	 *
	 *
	 * @param string $date
	 * @param string|null $timeZone
	 *
	 * @return false|string
	 *
	 * @throws \Exception
	 */

	public static function changeTimeZone( string $date, ?string $timeZone = null ) {
		if( empty( $timeZone ) === false ) {
			$time_zone = new DateTimeZone($timeZone);

			$dt = new DateTime($date,$time_zone);

			return $dt->format( 'Y-m-d H:i:s' );
		}
		else {
			return date("Y-m-d\TH:i:s\Z", strtotime( $date ) );
		}
	}

	/**
	 * returns and array of days back from today.
	 *
	 * @param	int		$countdownDays	number of days to get back
	 * @param	bool	$takeToday		whether or not take in count today
	 * @param	bool	$inverseOrder	whether is the array is descending or ascending
	 *
	 * @return	array
	 *
	 * @throws	\Exception
	 */

	public static function getCountdownArrayDays( int $countdownDays, $takeToday = true, bool $inverseOrder = false ): array {
		$interval = new DateInterval( 'P' . $countdownDays . 'D' );

		$timeObj = new DateTime( 'NOW' );

		$start_date = $timeObj->sub( $interval )->format( 'Y-m-d' );

		$end_date   = date( 'Y-m-d' );

		if( $inverseOrder === true ) {
			$days = self::getDatesByRangeInverse( $start_date, $end_date );

			if ( $takeToday === false ) {
				array_shift( $days );
			}
		}
		else {
			$days = self::getDatesByRange( $start_date, $end_date );

			if ( $takeToday === false ) {
				array_pop( $days );
			}
		}

		return array_values( $days );
	}

	/**
	 *
	 *
	 * @param int $countdownDays
	 *
	 * @return false|string
	 */

	public static function getDateFromNow( int $countdownDays ) {
		$time = time( ) - ( 86400 * (int)$countdownDays );

		return date( 'Y-m-d', $time );
	}
}